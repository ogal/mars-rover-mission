<?php

declare(strict_types=1);

namespace App\ValueObject;

/**
 * This class it is not actually necessary, due to ChoiceQuestion class providing 
 * a pre-defined options to choose. Must be deleted in Prod
 */
class Orientation
{
    private array $orientations = [
        'north' => 'N',
        'south' => 'S',
        'east' => 'E',
        'west' => 'W'
    ];

    public function __construct(string $orientation)
    {
        $this->isValidOrientation($orientation);
    }

    public function isValidOrientation(string $orientation): string
    {
        foreach ($this->orientations as $item) {
            if ($item === trim($orientation)) {
                echo $item . ' is a valid orientation';
                return $item;
            }
        }
        echo $orientation . '--> is not a valid orientation';
    }
}