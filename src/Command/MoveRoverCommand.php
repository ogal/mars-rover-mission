<?php

namespace App\Command;

use App\Entity\Rover;
use App\Service\RoverManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

class MoveRoverCommand extends Command
{
    protected static $defaultName = 'app:move-rover';
    protected static $defaultDescription = 'Send remote orders to our brand new Mars Rover';

    private $roverManager;

    public function __construct(RoverManager $roverManager)
    {
        $this->roverManager = $roverManager;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption('x_axis', 'x', InputOption::VALUE_REQUIRED, 'Where is the Rover located on the X aixs. Value must be under 200')
            ->addOption('y_axis', 'y', InputOption::VALUE_REQUIRED, 'Where is the Rover located on the Y aixs. Value must be under 200')
            ->setHelp('This command allows you to move the Rover through the planet surface.')
            ->addUsage('bin/console app:move-rover -x=78 -y=32')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $x_axis = substr($input->getOption('x_axis'), -3);
        $y_axis = substr($input->getOption('y_axis'), -3);

        $x_axis = str_replace('=', '', $x_axis);
        $y_axis = str_replace('=', '', $y_axis);
    
        if ($x_axis && $y_axis && $x_axis <= 200 && $y_axis <= 200) {

            $orientation = $this->getOrientation($input, $output);
        
            $rover = new Rover((int)$x_axis, (int)$y_axis, $orientation);

            $directions = $this->getDirectionString($input, $output);

            $this->roverManager->move($rover, $directions);

            $io->success('Rover went from ' . $x_axis . ' to current X position: ' . (($rover->getXAxis()>=200) ? "200. Can not go further." : $rover->getXAxis()) . PHP_EOL .  
                         'Rover went from ' . $y_axis . ' to current Y position: ' . (($rover->getYAxis()>=200) ? "200. Can not go further." : $rover->getYAxis()));

            return Command::SUCCESS;

        } else {
            
            $io->error('You have to set both X & Y coordinates and be below 200.');
            $io->comment('Try: bin/console app:move-rover --help');

            return Command::FAILURE;
        }
    }

    private function getOrientation(InputInterface $input, OutputInterface $output): string
    {
        $helper = $this->getHelper('question');
        
        $question = new ChoiceQuestion(
            'Please select orientation: "N" for Nord, "S" for South, "W" for West, "E" for East (defaults to N (Nord))',
            ['N', 'S', 'W', 'E'],
            0
        );

        $question->setErrorMessage('Orientation %s is invalid.');
    
        $or = $helper->ask($input, $output, $question);
        $output->writeln('You have selected: '. $or);

        return $or;
    }

    private function getDirectionString(InputInterface $input, OutputInterface $output): array
    {
        $helper = $this->getHelper('question');

        $question = new Question('Please enter the string of directions "F" for Front, "L" for Left, "R" for Right (defaults to FFF) ', 'FFF');

        $question->setNormalizer(function($normalizer) {
            return strtoupper(preg_replace("/[^frlFRL]/", "", $normalizer));
        });

        $directionString = $helper->ask($input, $output, $question);

        return str_split($directionString);
    }
}
