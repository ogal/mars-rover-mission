<?php

namespace App\Service;

use App\Entity\Rover;

class RoverManager
{
    public function move(Rover $rover, array $directions): void
    {
        switch($rover->getOrientation()){
            case 'S':
                foreach ($directions as $direction) {
                    if ($direction === 'F') {
                        $rover->setYAxis($rover->getYAxis() - 1);
                    } elseif ($direction === 'L') {
                        $rover->setXAxis($rover->getXAxis() - 1);
                    } elseif ($direction === 'R') {
                        $rover->setXAxis($rover->getXAxis() + 1);
                    }
                }
                break;
            case "W":
                foreach ($directions as $direction) {
                    if ($direction === 'F') {
                        $rover->setXAxis($rover->getXAxis() - 1);
                    } elseif ($direction === 'L') {
                        $rover->setYAxis($rover->getYAxis() - 1);
                    } elseif ($direction === 'R') {
                        $rover->setYAxis($rover->getYAxis() + 1);
                    }
                }
                break;
            case "E":
                foreach ($directions as $direction) {
                    if ($direction === 'F') {
                        $rover->setXAxis($rover->getXAxis() + 1);
                    } elseif ($direction === 'L') {
                        $rover->setYAxis($rover->getYAxis() - 1);
                    } elseif ($direction === 'R') {
                        $rover->setYAxis($rover->getYAxis() + 1);
                    }
                }
                break;
            default:        
                foreach ($directions as $direction) {
                    if ($direction === 'F') {
                        $rover->setYAxis($rover->getYAxis() + 1);
                    } elseif ($direction === 'L') {
                        $rover->setXAxis($rover->getXAxis() - 1);
                    } elseif ($direction === 'R') {
                        $rover->setXAxis($rover->getXAxis() + 1);
                    }
                }  
        }
    }
}
