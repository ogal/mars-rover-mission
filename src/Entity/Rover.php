<?php

declare(strict_types=1);

namespace App\Entity;

class Rover
{
    private int $xAxis = 0;
    private int $yAxis = 0;
    public $orientation;
    private const MAX_DISTANCE = 200; 

    public function __construct(int $xAxis, int $yAxis, string $orientation)
    {
        $this->xAxis = $xAxis;
        $this->yAxis = $yAxis;
        $this->orientation = $orientation;
    }    

    /**
     * Get the value of orientation
     */ 
    public function getOrientation()
    {
        return $this->orientation;
    }

    /**
     * Get the value of xAxis
     */ 
    public function getXAxis()
    {
        return $this->xAxis;
    }

    /**
     * Set the value of xAxis
     *
     * @return  self
     */ 
    public function setXAxis($xAxis)
    {
        if($xAxis > self::MAX_DISTANCE){
            return;
        }

        $this->xAxis = $xAxis;

        return $this;
    }

    /**
     * Get the value of yAxis
     */ 
    public function getYAxis()
    {
        return $this->yAxis;
    }

    /**
     * Set the value of yAxis
     *
     * @return  self
     */ 
    public function setYAxis($yAxis)
    {
        if($yAxis > self::MAX_DISTANCE){
            return;
        }

        $this->yAxis = $yAxis;

        return $this;
    }
}