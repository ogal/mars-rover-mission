<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class MoveRoverCommandTest extends KernelTestCase
{

    private function getCommand(): CommandTester
    {
        $kernel = self::bootKernel();

        $application = new Application($kernel);

        $command = $application->find('app:move-rover');

        $commandTester = new CommandTester($command);

        return $commandTester;
    }

    public function testFailiureWhenNotSettingYPosition(): void
    {
        $commandTester = $this->getCommand();
        
        $commandTester->execute([
            '--x_axis' => '23'
        ]);

        $output = $commandTester->getDisplay();

        $this->assertStringContainsString('You have to set both X & Y coordinates and be below 200', $output);
    }

    public function testFailiureWhenNotSettingXPosition(): void
    {
        $commandTester = $this->getCommand();

        $commandTester->execute([
            '--y_axis' => '59'
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('You have to set both X & Y coordinates and be below 200', $output);
    }

    public function testAxisCanNotBeBiggerThan200(): void
    {
        $commandTester = $this->getCommand();

        $commandTester->execute([
            '--x_axis' => '203',
            '--y_axis' => '59'
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('You have to set both X & Y coordinates and be below 200', $output);

        $commandTester->execute([
            '--x_axis' => '103',
            '--y_axis' => '400'
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('You have to set both X & Y coordinates and be below 200', $output);
    }

    public function testAssertingDefaultInputsValuesOrientationAndDirections(): void
    {
        $commandTester = $this->getCommand();

        $commandTester->execute([
            '--x_axis' => '188',
            '--y_axis' => '40'
        ]);

        $output = $commandTester->getDisplay();

        $this->assertStringContainsString('Rover went from', $output);
    }

    public function testRoverStopsBeforeGoingBeyond200WithDefValues(): void
    {
        $commandTester = $this->getCommand();

        // Default values face North and goes 3 steps forward, y_axis then sorpases 200 and fails 
        $commandTester->execute([
            '--x_axis' => '20',
            '--y_axis' => '199'
        ]);

        $output = $commandTester->getDisplay();

        $this->assertStringContainsString('200. Can not go further.', $output);
    }

    public function testRoverStopsBeforeGoingBeyond200WhenFacingEast(): void
    {
        $commandTester = $this->getCommand();

        $commandTester->setInputs(['E']);

        // Rover face East and goes 3 steps forward, x_axis then sorpases 200 and fails 
        $commandTester->execute([
            '--x_axis' => '199',
            '--y_axis' => '23'
        ]);

        $output = $commandTester->getDisplay();

        $this->assertStringContainsString('200. Can not go further.', $output);
    }
}
