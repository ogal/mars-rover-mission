# docker-dev-env-for-symfony

This repository contains the basic configuration for a complete local environment for Symfony projects

### Content:
- NGINX 1.19 container to handle HTTP requests
- PHP 8.0.1 container to host your Symfony application
- MySQL 8.0 container to store databases

(feel free to update any version in `Dockerfiles` and ports in `docker-compose.yml`)

### Installation:
- Rename docker-compose.yml.dist to docker-compose.yml 
- Run `make build` to create all containers 
- Run `make start` to spin up containers
- Enter the PHP container with `make ssh-be`
- Once in the container Run `composer install`
- Navigate to `localhost:1000` so you can see the Symfony welcome page :)

### Usage
- Start by running the tests `bin/phpunit --filter MoveRoverCommandTest`
- Run the command to send orders to the Mars Rover: 
See instructions and usage:
~~~
bin/console app:move-rover --help
~~~
Example of usage:
~~~
bin/console app:move-rover -x=3 -y=4
~~~
And follow the instructions on screen